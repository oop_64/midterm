public class Store {
    private char symbol;
    private String name;
    private int x;
    private int y;
    public static int x_min = 0;
    public static int x_max = 50;
    public static int y_min = 0;
    public static int y_max = 10;

    public Store(char symbol , String name,int x,int y){
        this.symbol = symbol;
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public int GetX(){
        return x;
    }

    public int GetY(){
        return y;
    }

    public char Getsymbol(){
        return symbol;
    }

    public String Getname(){
        return name;
    }

}

