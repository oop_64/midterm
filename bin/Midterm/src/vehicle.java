public class vehicle {
    private char symbol;
    private String name;
    private String type;
    private int x;
    private int y;
    public static int x_min = 0;
    public static int x_max = 50;
    public static int y_min = 0;
    public static int y_max = 10;

    public vehicle(String name,char symbol,String type,int x, int y){
        this.name = name;
        this.symbol = symbol;
        this.type = type;
        this.x =x;
        this.y =y;
    }

    public int GetX(){
        return x;
    }

    public int GetY(){
        return y;
    }

    public char Getsymbol(){
        return symbol;
    }

    public String Getname(){
        return name;
    }

    public String Gettype(){
        return type;
    }
}
