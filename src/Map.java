public class Map {
    private int width;
    private int height;

    public Map(int width,int height){
        this.height = height;
        this.width = width;
    }

    public void print(){
        for(int i = 0;i < height;i++){
            for(int j = 0;j < width;j++){
                System.out.print("-");
            }
        
            System.out.println();
        }
            
    }
}
